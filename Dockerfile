# install OS layer to your dockerfile
FROM Centos:7

# Label your dockerfile
LABEL authors="Hua Lin  hlin001@citymail.cuny.edu"

# install node v8 to run environment
RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -
RUN yum -y install nodejs wget

# install yarn in global mode
RUN npm install -g yarn

# set app folder env variables - as per standard it should follow /liveperson/code/name_of_project
ENV LP_HOME="/liveperson"
ENV APP_CODE="${LP_HOME}/code/agent_widget"

# create folder where application will be running
RUN mkdir -p ${APP_CODE}/

# install external dependencies
COPY package.json yarn.lock ${APP_CODE}/

# install your global dependencies
RUN cd ${APP_CODE}/ && \
    yarn install && \
    yarn chache clean && \

# copy files that are required for the app to work - modify the lines accordingly
COPY . .

# change working dir
WORKDIR ${APP_CODE}

# start server and provide port that you will expose
EXPOSE 3000
CMD yarn start ${APP_CODE}
